import tkinter as tk

from reflect import *
from smile import *
from eyes import *
from Stars import *
from RandomStars import *
from circle import *
from homedepotturtlegraphics import *
from pepsi import *
class MainMenu:
    def __init__(self, master,*args,**kwargs):
        self.master = master
        self.master.minsize(700, 700)
        self.master.wm_title(". . . MENU . . .")
        self.master.option_add("*Font", "helvetica")
        # start frame
        self.frame = tk.Frame(self.master, relief='raised', borderwidth=1, background="#eee8d5")
        self.frame.place(x=100, y=100,                    )
        self.button6 = tk.Button(self.frame, text = 'circle', width = 25, command = circle1)
        self.button6.pack() 
        self.button1 = tk.Button(self.frame, text = 'reflect', width = 25, command = mirror)
        self.button1.pack() 
        self.button2 = tk.Button(self.frame, text = 'eyes', width = 25, command = eyesmain)
        self.button2.pack()
        self.button3 = tk.Button(self.frame, text = 'stars', width = 25, command = Starsmain)
        self.button3.pack()
        self.button4 = tk.Button(self.frame, text = 'randomstars', width = 25, command = Stars)
        self.button4.pack()
        self.button5 = tk.Button(self.frame, text = 'Home depot', width = 25, command = homedepot)
        self.button5.pack()
        self.button7 = tk.Button(self.frame, text = 'pepsi', width = 25, command = pepsi)
        self.button7.pack()
        self.quitButton = tk.Button(self.frame, text = 'Quit', width = 25, command = self.close_windows)
        self.quitButton.pack()
        
        self.text = tk.Label(self.frame, text="* * * * * * * * * * * * * \n \
reflect \n  \
* * * * * * * * * * * * * \n  \
reflect \n \
{pop stack of activation records} \n \
* * * * * * * * * * * * * \n \
spiralgraph \n \
{turtle version with vector list) \n \
* * * * * * * * * * * * * \n \
pinkspiral \n \
(Create with an elipse funtion  x^2/a^2 + y^2/b^2 = 1) \n \
* * * * * * * * * * * * * \n \
sword \n \
Study of color.\n \
* * * * * * * * * * * * * \n \
Cw. Coleman",font=('courier', '10'),background="#eee8d5" )
        self.text.pack()
        self.frame.pack(expand=True, fill='both')
        # labels can be text or images
        # * * * * * * * * *

    def new_window(self):
        self.newWindow = tk.Toplevel(self.master)
        self.app = ChoasWindow(self.newWindow)
    def close_windows(self):
        self.master.destroy()

class ChoasWindow:
    def __init__(self, master):
        self.master = master
        self.frame = tk.Frame(self.master)
        self.quitButton = tk.Button(self.frame, text = 'Quit', width = 25, command = self.close_windows)
        self.quitButton.pack()
        self.frame.pack()
    def close_windows(self):
        self.master.destroy()

def main():
    root = tk.Tk()
    import sys
    print(sys.version)
    app = MainMenu(root)
    root.mainloop()

if __name__ == '__main__':
    main()


"""
apt install python3-pil python-pil python3-pil.imagetk python-pil.imagetk;
apt install python3-pil python-pil
apt install python3-pil.imagetk
apt install python-pil.imagetk

https://pythonprogramming.net/tkinter-adding-text-images/
https://stackoverflow.com/questions/17466561/best-way-to-structure-a-tkinter-application
https://stackoverflow.com/questions/23901168/how-do-i-insert-a-jpeg-image-into-a-python-tkinter-window
$base03:    #002b36; (dark grey)
$base02:    #073642;
$base01:    #586e75;
$base00:    #657b83;
$base0:     #839496;
$base1:     #93a1a1;
$base2:     #eee8d5; (cream)
$base3:     #fdf6e3;
$yellow:    #b58900;
$orange:    #cb4b16;
$red:       #dc322f;
$magenta:   #d33682;
$violet:    #6c71c4;
$blue:      #268bd2;
$cyan:      #2aa198;
$green:     #859900;

"""
