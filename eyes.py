import turtle
import random
def eyesmain():
	def eyes():
		turtle.speed(10)
		w = turtle.Screen()
		w.clear()
		w.bgcolor("#0C0C0D")
		screen = turtle.Screen()
		#turtle.tracer(0, 0)
		
		#random_number = random.randint(0,16777215)
		#hex_number = str(hex(random_number))
		#hex_number ='#'+ hex_number[2:]
		
		#First eye
		t = turtle.Turtle()
		t.pencolor("#77C9FF")
		t.penup()
		t.goto(200, 0)
		t.pendown()
		t.dot(200)
		
		t.pencolor("#31A0E9")
		t.penup()
		t.goto(200, 0)
		t.pendown()
		t.dot(170)
		
		t.pencolor("#77C9FF")
		t.penup()
		t.goto(200, 0)
		t.pendown()
		t.dot(150)
		
		t.pencolor("#000000")
		t.penup()
		t.goto(210, 5)
		t.pendown()
		t.dot(120)
		
		t.pencolor("#023658")
		t.penup()
		t.goto(240, 10)
		t.pendown()
		t.dot(80)
		
		t.pencolor("#1A6DA4")
		t.penup()
		t.goto(180, -20)
		t.pendown()
		t.dot(20)
		
		#Second eye
		t = turtle.Turtle()
		t.pencolor("#77C9FF")
		t.penup()
		t.goto(-300, 0)
		t.pendown()
		t.dot(200)
		
		t.pencolor("#31A0E9")
		t.penup()
		t.goto(-300, 0)
		t.pendown()
		t.dot(170)
		
		t.pencolor("#77C9FF")
		t.penup()
		t.goto(-300, 0)
		t.pendown()
		t.dot(150)
		
		t.pencolor("#000000")
		t.penup()
		t.goto(-290, 5)
		t.pendown()
		t.dot(120)
		
		t.pencolor("#023658")
		t.penup()
		t.goto(-260, 10)
		t.pendown()
		t.dot(80)
		
		t.pencolor("#1A6DA4")
		t.penup()
		t.goto(-320, -20)
		t.pendown()
		t.dot(20)
		
		t.penup()
		t.goto(1000, -20)
			
		
		
	def flatoval(): 
		turtle.speed(10)
		w = turtle.Screen()
		w.bgcolor("#0C0C0D")
		screen = turtle.Screen()
		#turtle.tracer(0, 0)
		
		#random_number = random.randint(0,16777215)
		#hex_number = str(hex(random_number))
		#hex_number ='#'+ hex_number[2:]
		
		
		t = turtle.Turtle()
		t2 = turtle.Turtle()
		t3 = turtle.Turtle()
		t4 = turtle.Turtle()
		t.speed(10)
		t2.speed(10)
		t3.speed(10)
		t4.speed(10)
		t.pencolor("#08436B")
		t2.pencolor("#08436B")
		t3.pencolor("#08436B")
		t4.pencolor("#08436B")
		t.penup()
		t2.penup()
		t3.penup()
		t4.penup()
		t.pensize(5)
		t2.pensize(5)
		t3.pensize(5)
		t4.pensize(5)
		t.goto(400, -120)
		t2.goto(0, 80)
		t3.goto(-500, 80)
		t4.goto(-100, -120)
		t.pendown()
		t2.pendown()
		t3.pendown()
		t4.pendown()
		for loop in range(50):
			
			t.lt(79)
			t.fd(400)
			
			t2.lt(79)
			t2.fd(-400)
			
			t3.lt(79)
			t3.fd(-400)
			
			t4.lt(79)
			t4.fd(400)
		
	def Remove(): 
		turtle.speed(10)
		w = turtle.Screen()
		w.bgcolor("#0C0C0D")
		screen = turtle.Screen()
		#turtle.tracer(0, 0)
		
		#random_number = random.randint(0,16777215)
		#hex_number = str(hex(random_number))
		#hex_number ='#'+ hex_number[2:]
		
		
		t = turtle.Turtle()
		t.speed(10)
		t.pencolor("#0C0C0D")
		t.fillcolor("#0C0C0D")
		t.penup()
		t.goto(-960, 80)
		t.begin_fill()
		t.pendown()
		t.lt(90)
		t.fd(413)
		t.rt(90)
		t.fd(1920)
		t.rt(90)
		t.fd(413)
		t.rt(90)
		t.fd(1930)
		t.end_fill()
		
	def Remove2(): 
		turtle.speed(10)
		w = turtle.Screen()
		w.bgcolor("#0C0C0D")
		screen = turtle.Screen()
		#turtle.tracer(0, 0)
		
		#random_number = random.randint(0,16777215)
		#hex_number = str(hex(random_number))
		#hex_number ='#'+ hex_number[2:]
		
		
		t = turtle.Turtle()
		t.speed(10)
		t.pencolor("#0C0C0D")
		t.fillcolor("#0C0C0D")
		t.penup()
		t.goto(-960, -120)
		t.begin_fill()
		t.pendown()
		t.rt(90)
		t.fd(410)
		t.lt(90)
		t.fd(1920)
		t.lt(90)
		t.fd(410)
		t.lt(90)
		t.fd(1920)
		t.end_fill()
		
	def Remove3(): 
		turtle.speed(10)
		w = turtle.Screen()
		w.bgcolor("#0C0C0D")
		screen = turtle.Screen()
		#turtle.tracer(0, 0)
		
		#random_number = random.randint(0,16777215)
		#hex_number = str(hex(random_number))
		#hex_number ='#'+ hex_number[2:]
		
		
		t = turtle.Turtle()
		t.speed(10)
		t.pencolor("#0C0C0D")
		t.fillcolor("#0C0C0D")
		t.penup()
		t.goto(-500, 100)
		t.begin_fill()
		t.pendown()
		t.rt(90)
		t.fd(300)
		t.rt(90)
		t.fd(400)
		t.rt(90)
		t.fd(300)
		t.rt(90)
		t.fd(400)
		t.end_fill()
		
	def Remove4(): 
		turtle.speed(10)
		w = turtle.Screen()
		w.bgcolor("#0C0C0D")
		screen = turtle.Screen()
		#turtle.tracer(0, 0)
		
		#random_number = random.randint(0,16777215)
		#hex_number = str(hex(random_number))
		#hex_number ='#'+ hex_number[2:]
		
		
		t = turtle.Turtle()
		t.speed(10)
		t.pencolor("#0C0C0D")
		t.fillcolor("#0C0C0D")
		t.penup()
		t.goto(400, 100)
		t.begin_fill()
		t.pendown()
		t.rt(90)
		t.fd(300)
		t.lt(90)
		t.fd(400)
		t.lt(90)
		t.fd(300)
		t.lt(90)
		t.fd(400)
		t.end_fill()
		
		
		screen.exitonclick()
	try:
		eyes()
		flatoval()
		Remove()
		Remove2()
		Remove3()
		Remove4()
	finally:
		turtle.Terminator()
if __name__ == "__main__":
	eyesmain()
#lt = left turn
#rt = right turn
#bk = back
